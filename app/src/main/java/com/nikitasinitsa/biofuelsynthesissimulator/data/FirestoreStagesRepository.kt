package com.nikitasinitsa.biofuelsynthesissimulator.data

import com.google.firebase.firestore.FirebaseFirestore
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.Stage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.repositories.StagesRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class FirestoreStagesRepository : StagesRepository {

    private val firestore = FirebaseFirestore.getInstance()

    override fun getStages(): Single<List<Stage>> =
        Single.create { emitter ->
            firestore.collection(References.STAGES).orderBy("id").get()
                .addOnSuccessListener { snapshot ->
                    if (!emitter.isDisposed) {
                        emitter.onSuccess(snapshot.toObjects(Stage::class.java))
                    }
                }
                .addOnFailureListener {
                    if (!emitter.isDisposed) {
                        emitter.onError(it)
                    }
                }
        }

    override fun observeCurrentStage(): Observable<CurrentStage> =
        Observable.create { emitter ->
            firestore.collection(References.CURRENT_STAGE).document(References.CURRENT_STAGE)
                .addSnapshotListener { documentSnapshot, firebaseFirestoreException ->
                    if (firebaseFirestoreException == null) {
                        if (documentSnapshot != null) {
                            val currentStage = documentSnapshot.toObject(CurrentStage::class.java)
                            emitter.onNext(
                                currentStage ?: CurrentStage()
                            )
                        } else {
                            if (!emitter.isDisposed) {
                                emitter.onNext(CurrentStage())
                            }
                        }
                    } else {
                        if (!emitter.isDisposed) {
                            emitter.onError(firebaseFirestoreException)
                        }
                    }
                }
        }

    override fun updateCurrentStage(stage: CurrentStage): Completable =
        Completable.create { emitter ->
            firestore.collection(References.CURRENT_STAGE).document(References.CURRENT_STAGE)
                .set(stage)
                .addOnCompleteListener {
                    if (!emitter.isDisposed) {
                        emitter.onComplete()
                    }
                }
                .addOnFailureListener {
                    if (!emitter.isDisposed) {
                        emitter.onError(it)
                    }
                }
        }

    override fun deleteCurrentStage(): Completable =
        Completable.create { emitter ->
            firestore.collection(References.CURRENT_STAGE).document(References.CURRENT_STAGE)
                .delete()
                .addOnCompleteListener {
                    if (!emitter.isDisposed) {
                        emitter.onComplete()
                    }
                }
                .addOnFailureListener {
                    if (!emitter.isDisposed) {
                        emitter.onError(it)
                    }
                }
        }
}