package com.nikitasinitsa.biofuelsynthesissimulator.data

object References {
    const val STAGES = "stages"
    const val CURRENT_STAGE = "currentStage"
}