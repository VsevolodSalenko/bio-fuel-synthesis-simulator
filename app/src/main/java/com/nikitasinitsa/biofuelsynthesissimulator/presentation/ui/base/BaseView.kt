package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base

interface BaseView {
    fun showInternalError()
    fun displayOfflineState()
    fun displayOnlineState()
}