package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base

interface BaseDialogView {
    fun dismiss()
    fun showInternalError()
}