package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.utils

object DurationUtils {
    fun toMinutes(millis: Long): Int = (millis.toFloat() / 60 / 1000).toInt()
    fun toSeconds(millis: Long): Int = (millis / 1000).toInt()
}