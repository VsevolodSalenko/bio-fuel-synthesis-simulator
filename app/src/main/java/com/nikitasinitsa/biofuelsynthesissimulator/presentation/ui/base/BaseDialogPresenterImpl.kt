package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base

import android.util.Log
import com.crashlytics.android.Crashlytics
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

open class BaseDialogPresenterImpl<View : BaseDialogView>(protected val appBridge: AppBridge) :
    DialogPresenter<View> {

    protected val disposables = CompositeDisposable()
    private var viewReference: WeakReference<View>? = null

    protected fun getView(): View? = viewReference?.get()

    override fun onStart(view: View) {
        viewReference = WeakReference(view)
    }

    override fun onStop() {
        viewReference?.clear()
        viewReference = null
        disposables.clear()
    }

    protected fun showInternalError(error: Throwable) {
        Crashlytics.logException(error)
        Log.e("internalError", "", error)
        getView()?.showInternalError()
    }
}