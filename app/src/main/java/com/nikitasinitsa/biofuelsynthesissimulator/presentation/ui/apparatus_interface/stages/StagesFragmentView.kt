package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages

import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.Stage
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseView

interface StagesFragmentView : BaseView {
    fun displayStages(stages: List<Stage>)
    fun updateCurrentStage(currentStage: CurrentStage)
    fun switchCurrentStage(currentStage: CurrentStage)
    fun showStartSynthesisScreen()
    fun startSynthesis(currentStage: CurrentStage)
    fun showSynthesisCompletedDialog()
}