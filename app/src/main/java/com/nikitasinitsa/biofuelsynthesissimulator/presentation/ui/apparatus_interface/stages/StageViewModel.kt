package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages

import com.nikitasinitsa.biofuelsynthesissimulator.domain.Identifiable
import com.nikitasinitsa.biofuelsynthesissimulator.domain.VersionComparable
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.Stage

class StageViewModel(
    var stage: Stage = Stage(),
    var state: StageViewState = StageViewState.PENDING,
    var progressSeconds: Int = -1,
    override var id: Long = stage.id
) : Identifiable, VersionComparable<StageViewModel> {

    override fun isChanged(anotherVersion: StageViewModel): Boolean =
        stage.isChanged(anotherVersion.stage)
                || state != anotherVersion.state
                || progressSeconds != anotherVersion.progressSeconds

    override fun update(anotherVersion: StageViewModel) {
        stage.update(anotherVersion.stage)
        state = anotherVersion.state
        progressSeconds = anotherVersion.progressSeconds
    }
}