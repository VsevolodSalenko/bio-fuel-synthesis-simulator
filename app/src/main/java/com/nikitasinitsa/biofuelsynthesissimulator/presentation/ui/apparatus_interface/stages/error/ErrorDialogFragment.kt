package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.error


import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikitasinitsa.biofuelsynthesissimulator.R
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseDialogFragment
import kotlinx.android.synthetic.main.fragment_error.*

class ErrorDialogFragment : BaseDialogFragment(), ErrorFragmentView {

    companion object {

        private const val CURRENT_STAGE_ARG = "stageName"

        fun newInstance(currentStage: CurrentStage): ErrorDialogFragment =
            ErrorDialogFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(CURRENT_STAGE_ARG, currentStage)
                }
            }
    }

    private lateinit var currentStage: CurrentStage
    private lateinit var presenter: ErrorFragmentPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentStage = arguments!!.getParcelable(CURRENT_STAGE_ARG)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_error, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appBridge = requireActivity().application as AppBridge
        presenter = ErrorFragmentPresenterImpl(appBridge)
        errorTextView.text = getString(R.string.error_occurred, currentStage.stage.name)
        fixErrorButton.setOnClickListener {
            fixError()
        }
        fixedButton.setOnClickListener {
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun onErrorFixed() {
        fixErrorProgressBar.visibility = View.GONE
        errorContainer.visibility = View.GONE
        errorFixedContainer.visibility = View.VISIBLE
    }

    private fun fixError() {
        fixErrorProgressBar.visibility = View.VISIBLE
        errorContainer.visibility = View.INVISIBLE
        errorFixedContainer.visibility = View.GONE
        val timer = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}

            override fun onFinish() {
                fixErrorProgressBar.visibility = View.VISIBLE
                errorContainer.visibility = View.GONE
                errorFixedContainer.visibility = View.INVISIBLE
                presenter.onErrorFixed(currentStage)
                cancel()
            }
        }
        timer.start()
    }
}
