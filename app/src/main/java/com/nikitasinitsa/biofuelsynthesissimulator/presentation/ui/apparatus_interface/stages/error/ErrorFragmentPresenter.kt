package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.error

import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.DialogPresenter

interface ErrorFragmentPresenter : DialogPresenter<ErrorFragmentView> {
    fun onErrorFixed(currentStage: CurrentStage)
}