package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages


import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.animation.doOnEnd
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nikitasinitsa.biofuelsynthesissimulator.R
import com.nikitasinitsa.biofuelsynthesissimulator.device.NetworkStateProvider
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.Stage
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.error.ErrorDialogFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.synthesis_completed.SynthesisCompletedFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.start_synthesis.StartSynthesisFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main.ToolbarState
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.utils.DurationUtils
import kotlinx.android.synthetic.main.fragment_stages.*


class StagesFragment : BaseFragment(), StagesFragmentView {

    override val toolbarState: ToolbarState = ToolbarState.APPARATUS_INTERFACE
    private lateinit var presenter: StagesFragmentPresenter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var errorFragmentDialog: ErrorDialogFragment? = null
    private var progressAnimator: ValueAnimator? = null
    private var completeDialogFragment: SynthesisCompletedFragment? = null
    private var endedBecauseCancelling = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_stages, container, false)


    private val adapter = StagesAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = StagesFragmentPresenterImpl(appBridge, NetworkStateProvider(requireActivity()))
        stagesRecyclerView.adapter = adapter
        linearLayoutManager = LinearLayoutManager(requireContext())
        stagesRecyclerView.layoutManager = linearLayoutManager
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun displayOfflineState() {
        stagesRecyclerView.visibility = View.INVISIBLE
        stagesProgressBar.visibility = View.VISIBLE
    }

    override fun displayOnlineState() {
        stagesProgressBar.visibility = View.GONE
        stagesRecyclerView.visibility = View.VISIBLE
    }

    override fun displayStages(stages: List<Stage>) {
        adapter.displayStages(stages.map(StageViewModelMapper::map))
    }

    override fun updateCurrentStage(currentStage: CurrentStage) {
        if (checkError(currentStage)) {
            startSynthesis(currentStage)
        }
    }

    private fun checkError(currentStage: CurrentStage) =
        if (currentStage.errorOccurredMillis != -1L) {
            showError(currentStage)
            progressAnimator?.pause()
            completeDialogFragment?.dismiss()
            false
        } else {
            true
        }

    private fun showError(currentStage: CurrentStage) {
        errorFragmentDialog?.dismiss()
        errorFragmentDialog = ErrorDialogFragment.newInstance(currentStage)
        errorFragmentDialog!!.isCancelable = false
        errorFragmentDialog!!.show(fragmentManager!!, null)
    }

    private fun convertDpToPx(dp: Float) =
        (dp * requireActivity().resources.displayMetrics.density).toInt()


    override fun switchCurrentStage(currentStage: CurrentStage) {
        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                super.onItemRangeChanged(positionStart, itemCount)
                adapter.unregisterAdapterDataObserver(this)
                val currentStagePosition = adapter.getCurrentStagePosition()
                if (currentStagePosition != -1) {
                    linearLayoutManager.scrollToPositionWithOffset(
                        currentStagePosition,
                        convertDpToPx(32f)
                    )
                }
            }
        })
        adapter.switchStage(StageViewModelMapper.map(currentStage))
        if (checkError(currentStage)) {
            startSynthesis(currentStage)
        }
    }

    override fun startSynthesis(currentStage: CurrentStage) {
        val remainMillis = currentStage.getRemainMillis()
        if (remainMillis > 0) {
            progressAnimator?.let {
                endedBecauseCancelling = true
                progressAnimator?.cancel()
            }
            progressAnimator = ValueAnimator.ofInt(0, DurationUtils.toSeconds(remainMillis))
            with(progressAnimator!!) {
                duration = remainMillis
                addUpdateListener {
                    adapter.updateCurrentStage(StageViewModelMapper.map(currentStage).apply {
                        progressSeconds =
                            DurationUtils.toSeconds(currentStage.stage.durationMillis - currentStage.getRemainMillis())
                    })
                }
                doOnEnd {
                    if (!endedBecauseCancelling) {
                        presenter.switchToNextStage()
                        progressAnimator = null
                    } else {
                        endedBecauseCancelling = false
                    }
                }
                start()
            }
        } else {
            presenter.switchToNextStage()
        }
    }

    override fun showStartSynthesisScreen() {
        completeDialogFragment?.dismiss()
        errorFragmentDialog?.dismiss()
        fragmentManager!!.beginTransaction()
            .replace(R.id.mainActivityContainer, StartSynthesisFragment()).commit()
    }

    override fun showSynthesisCompletedDialog() {
        progressAnimator?.pause()
        errorFragmentDialog?.dismiss()
        completeDialogFragment?.dismiss()
        completeDialogFragment = SynthesisCompletedFragment()
        completeDialogFragment!!.isCancelable = false
        completeDialogFragment!!.show(fragmentManager!!, null)
    }
}
