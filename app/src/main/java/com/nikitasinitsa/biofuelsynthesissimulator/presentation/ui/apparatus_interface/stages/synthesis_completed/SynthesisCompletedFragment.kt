package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.synthesis_completed


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikitasinitsa.biofuelsynthesissimulator.R
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseDialogFragment
import kotlinx.android.synthetic.main.fragment_synthesis_completed.*

class SynthesisCompletedFragment : BaseDialogFragment(), SynthesisCompletedFragmentView {
    private lateinit var presenter: SynthesisCompletedFragmentPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_synthesis_completed, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = SynthesisCompletedFragmentPresenterImpl(appBridge)
        synthesisFinishedButton.setOnClickListener { presenter.onFinishButtonClicked() }
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }
}
