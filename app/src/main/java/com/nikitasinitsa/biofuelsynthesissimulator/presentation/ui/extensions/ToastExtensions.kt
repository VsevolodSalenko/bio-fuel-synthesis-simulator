package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.extensions

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.nikitasinitsa.biofuelsynthesissimulator.R

fun Context.showInternalErrorToast() {
    Toast.makeText(this, getString(R.string.application_internal_error), Toast.LENGTH_SHORT).show()
}

fun Fragment.showInternalErrorToast() {
    requireActivity().showInternalErrorToast()
}