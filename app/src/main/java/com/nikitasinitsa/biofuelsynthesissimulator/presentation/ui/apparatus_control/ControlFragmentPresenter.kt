package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_control

import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.Presenter

interface ControlFragmentPresenter : Presenter<ControlFragmentView> {
    fun onCancelButtonClicked()
    fun onSkipButtonClicked()
    fun onSimulateErrorButtonClicked()
}