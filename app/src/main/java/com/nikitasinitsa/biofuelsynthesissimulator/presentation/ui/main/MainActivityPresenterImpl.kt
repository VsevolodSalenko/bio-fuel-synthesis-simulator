package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main

import com.nikitasinitsa.biofuelsynthesissimulator.device.NetworkStateProvider
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BasePresenterImpl

class MainActivityPresenterImpl(
    appBridge: AppBridge,
    networkStateProvider: NetworkStateProvider
) : BasePresenterImpl<MainActivityView>(appBridge, networkStateProvider), MainActivityPresenter {

    override fun onStart(view: MainActivityView) {
        super.onStart(view)
    }

}