package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.error

import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseDialogView

interface ErrorFragmentView : BaseDialogView {
    fun onErrorFixed()
}