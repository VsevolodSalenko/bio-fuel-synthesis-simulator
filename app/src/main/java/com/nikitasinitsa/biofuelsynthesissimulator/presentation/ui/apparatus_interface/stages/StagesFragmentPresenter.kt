package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages

import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.Presenter

interface StagesFragmentPresenter : Presenter<StagesFragmentView> {
    fun switchToNextStage()
}