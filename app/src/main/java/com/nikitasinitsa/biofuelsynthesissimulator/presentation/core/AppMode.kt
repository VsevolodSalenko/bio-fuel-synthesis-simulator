package com.nikitasinitsa.biofuelsynthesissimulator.presentation.core

enum class AppMode {
    INTERFACE_MODE, CONTROL_MODE
}