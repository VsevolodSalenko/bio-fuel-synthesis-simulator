package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages

import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.Stage

object StageViewModelMapper {
    fun map(stage: Stage) = StageViewModel(stage, StageViewState.PENDING, -1)
    fun map(currentStage: CurrentStage) =
        StageViewModel(
            currentStage.stage,
            StageViewState.CURRENT,
            -1
        )
}