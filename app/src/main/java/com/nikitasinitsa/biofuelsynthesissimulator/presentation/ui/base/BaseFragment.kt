package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.extensions.showInternalErrorToast
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main.ConfigurableToolbar
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main.ToolbarState

abstract class BaseFragment : Fragment(), BaseView {
    protected lateinit var appBridge: AppBridge
    protected lateinit var configurableToolbar: ConfigurableToolbar

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configurableToolbar = activity as ConfigurableToolbar
        appBridge = requireActivity().application as AppBridge
        configurableToolbar.configureToolbar(toolbarState)
    }

    override fun showInternalError() {
        showInternalErrorToast()
    }

    protected abstract val toolbarState: ToolbarState
}