package com.nikitasinitsa.biofuelsynthesissimulator.presentation.core

import com.nikitasinitsa.biofuelsynthesissimulator.domain.repositories.StagesRepository

interface AppBridge {
    var stagesRepository: StagesRepository
    var prefs: Prefs
}