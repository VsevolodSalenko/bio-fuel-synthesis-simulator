package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base

interface Presenter<View : BaseView> {
    fun onStart(view: View)
    fun onStop()
}