package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_control

import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseView

interface ControlFragmentView : BaseView {
    fun updateCurrentStage(currentStage: CurrentStage)
}