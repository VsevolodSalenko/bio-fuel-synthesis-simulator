package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main

import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.Presenter

interface MainActivityPresenter : Presenter<MainActivityView> {

}