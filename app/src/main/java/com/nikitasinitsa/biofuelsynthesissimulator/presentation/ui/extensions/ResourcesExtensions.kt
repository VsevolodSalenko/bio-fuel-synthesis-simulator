package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.extensions

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import java.util.*

fun Context.getRussianResources(): Resources {
    val configuration = Configuration(resources.configuration)
    configuration.setLocale(Locale("ru"))
    return createConfigurationContext(configuration).resources
}