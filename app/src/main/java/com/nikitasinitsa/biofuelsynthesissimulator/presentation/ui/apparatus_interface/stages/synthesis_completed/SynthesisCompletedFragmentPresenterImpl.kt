package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.synthesis_completed

import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.DeleteCurrentStageUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseDialogPresenterImpl

class SynthesisCompletedFragmentPresenterImpl(appBridge: AppBridge) :
    BaseDialogPresenterImpl<SynthesisCompletedFragmentView>(appBridge),
    SynthesisCompletedFragmentPresenter {

    private val deleteCurrentStageUseCase = DeleteCurrentStageUseCase(appBridge.stagesRepository)

    override fun onFinishButtonClicked() {
        disposables.add(
            deleteCurrentStageUseCase.execute(null).subscribe(
                {
                    getView()?.dismiss()
                },
                ::showInternalError
            )
        )
    }

}