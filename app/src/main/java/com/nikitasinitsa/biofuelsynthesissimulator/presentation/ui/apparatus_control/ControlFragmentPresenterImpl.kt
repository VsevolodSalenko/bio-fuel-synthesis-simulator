package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_control

import com.nikitasinitsa.biofuelsynthesissimulator.device.NetworkStateProvider
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.Stage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.DeleteCurrentStageUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.GetStagesUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.ObserveCurrentStageUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.UpdateCurrentStageUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BasePresenterImpl

class ControlFragmentPresenterImpl(
    appBridge: AppBridge,
    networkStateProvider: NetworkStateProvider
) : BasePresenterImpl<ControlFragmentView>(appBridge, networkStateProvider),
    ControlFragmentPresenter {

    private val deleteCurrentStageUseCase = DeleteCurrentStageUseCase(appBridge.stagesRepository)
    private val updateCurrentStageUseCase = UpdateCurrentStageUseCase(appBridge.stagesRepository)
    private val observeCurrentStageUseCase = ObserveCurrentStageUseCase(appBridge.stagesRepository)
    private val getStagesUseCase = GetStagesUseCase(appBridge.stagesRepository)

    private val stages = ArrayList<Stage>()
    private var currentStage: CurrentStage? = null

    override fun onStart(view: ControlFragmentView) {
        super.onStart(view)
        disposables.add(
            getStagesUseCase.execute(null).subscribe(
                {
                    stages.clear()
                    stages.addAll(it)
                    disposables.add(
                        observeCurrentStageUseCase.execute(null).subscribe(
                            { stage ->
                                currentStage = stage
                                getView()?.updateCurrentStage(stage)
                            },
                            ::showInternalError
                        )
                    )
                },
                ::showInternalError
            )
        )
    }

    override fun onCancelButtonClicked() {
        deleteCurrentStage()
    }

    override fun onSkipButtonClicked() {
        val nextStage = getNextStage()
        if (nextStage != null) {
            disposables.add(
                updateCurrentStageUseCase.execute(
                    CurrentStage(
                        nextStage,
                        System.currentTimeMillis()
                    )
                ).subscribe(
                    {},
                    ::showInternalError
                )
            )
        } else {
            currentStage?.let {
                it.startMillis = System.currentTimeMillis() - it.stage.durationMillis
                disposables.add(
                    updateCurrentStageUseCase.execute(it).subscribe(
                        {},
                        ::showInternalError
                    )
                )
            }
        }
    }

    override fun onSimulateErrorButtonClicked() {
        currentStage!!.errorOccurredMillis = System.currentTimeMillis()
        disposables.add(
            updateCurrentStageUseCase.execute(currentStage!!).subscribe(
                {},
                ::showInternalError
            )
        )
    }

    private fun getNextStage(): Stage? {
        val currentStageIndex = stages.indexOf(currentStage!!.stage)
        return if (currentStageIndex < stages.size - 1) {
            stages[currentStageIndex + 1]
        } else {
            null
        }
    }

    private fun deleteCurrentStage() {
        disposables.add(
            deleteCurrentStageUseCase.execute(null).subscribe(
                {},
                ::showInternalError
            )
        )
    }
}