package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.extensions.showInternalErrorToast

abstract class BaseDialogFragment : DialogFragment(), BaseDialogView {
    protected lateinit var appBridge: AppBridge

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appBridge = requireActivity().application as AppBridge
    }

    override fun showInternalError() {
        showInternalErrorToast()
    }
}