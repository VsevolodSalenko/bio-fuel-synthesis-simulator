package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages

import com.nikitasinitsa.biofuelsynthesissimulator.device.NetworkStateProvider
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.Stage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.GetStagesUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.ObserveCurrentStageUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.UpdateCurrentStageUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BasePresenterImpl

class StagesFragmentPresenterImpl(
    appBridge: AppBridge,
    networkStateProvider: NetworkStateProvider
) : BasePresenterImpl<StagesFragmentView>(appBridge, networkStateProvider),
    StagesFragmentPresenter {

    private val observeCurrentStageUseCase = ObserveCurrentStageUseCase(appBridge.stagesRepository)
    private val getStagesUseCase = GetStagesUseCase(appBridge.stagesRepository)
    private val updateCurrentStageUseCase = UpdateCurrentStageUseCase(appBridge.stagesRepository)

    private var currentStage: CurrentStage? = null
    private var stages: List<Stage>? = null

    override fun switchToNextStage() {
        val nextStage = getNextStage()
        if (nextStage != null) {
            disposables.add(
                updateCurrentStageUseCase.execute(
                    CurrentStage(
                        nextStage,
                        System.currentTimeMillis()
                    )
                ).subscribe(
                    {},
                    this::showInternalError
                )
            )
        } else {
            getView()?.showSynthesisCompletedDialog()
        }
    }

    private fun getNextStage(): Stage? {
        val currentStageIndex = stages!!.indexOf(currentStage!!.stage)
        return if (currentStageIndex < stages!!.size - 1) {
            stages!![currentStageIndex + 1]
        } else {
            null
        }
    }

    override fun onOnlineState() {
        if (disposables.size() == 1) {
            disposables.add(
                getStagesUseCase.execute(null).subscribe(
                    { loadedStages ->
                        if (stages == null) {
                            stages = loadedStages
                            getView()?.displayOnlineState()
                            getView()?.displayStages(loadedStages)
                        }
                        startObservingCurrentStage()
                    },
                    this::showInternalError
                )
            )
        } else {
            super.onOnlineState()
        }
    }

    private fun startObservingCurrentStage() {
        disposables.add(
            observeCurrentStageUseCase.execute(null).subscribe(
                { loadedCurrentStage ->
                    handleCurrentStageUpdate(loadedCurrentStage)
                },
                this::showInternalError
            )
        )
    }

    private fun handleCurrentStageUpdate(updatedCurrentStage: CurrentStage) {
        val previousStage = currentStage
        val hasCurrentStage = hasCurrentStage()
        currentStage = updatedCurrentStage
        if (updatedCurrentStage.stage.id != -1L) {
            if (hasCurrentStage && previousStage!! == updatedCurrentStage) {
                getView()?.updateCurrentStage(updatedCurrentStage)
            } else {
                getView()?.switchCurrentStage(updatedCurrentStage)
            }
        } else {
            if (hasCurrentStage) {
                getView()?.showStartSynthesisScreen()
            } else {
                startSynthesis()
            }
        }
    }

    private fun startSynthesis() {
        val currentStage = CurrentStage(
            stages!!.first(),
            System.currentTimeMillis()
        )
        disposables.add(
            updateCurrentStageUseCase.execute(
                currentStage
            ).subscribe(
                {},
                this::showInternalError
            )
        )
    }

    private fun hasCurrentStage() = currentStage != null && currentStage!!.stage.id != -1L
}