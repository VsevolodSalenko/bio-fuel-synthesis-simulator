package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_control


import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.nikitasinitsa.biofuelsynthesissimulator.R
import com.nikitasinitsa.biofuelsynthesissimulator.device.NetworkStateProvider
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.extensions.getRussianResources
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main.ToolbarState
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.utils.DurationUtils
import kotlinx.android.synthetic.main.fragment_control.*
import kotlinx.android.synthetic.main.item_stage.*

class ControlFragment : BaseFragment(), ControlFragmentView {

    override val toolbarState: ToolbarState = ToolbarState.APPARATUS_CONTROL

    private lateinit var presenter: ControlFragmentPresenter
    private var progressAnimator: ValueAnimator? = null
    private var isProgressAnimatorCancelling = false
    private var currentStage: CurrentStage? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_control, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = ControlFragmentPresenterImpl(appBridge, NetworkStateProvider(requireActivity()))
        skipButton.setOnClickListener {
            presenter.onSkipButtonClicked()
        }
        cancelButton.setOnClickListener {
            presenter.onCancelButtonClicked()
        }
        simulateErrorButton.setOnClickListener {
            presenter.onSimulateErrorButtonClicked()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop()
    }

    override fun updateCurrentStage(currentStage: CurrentStage) {
        this.currentStage = currentStage
        if (currentStage.stage.id != -1L) {
            waitingForInputTextView.visibility = View.GONE
            controlContainer.visibility = View.VISIBLE
            inflateCurrentStage(currentStage)
            if (currentStage.errorOccurredMillis == -1L) {
                startAnimator(currentStage)
            } else {
                progressAnimator?.pause()
            }
        } else {
            waitingForInputTextView.visibility = View.VISIBLE
            controlContainer.visibility = View.GONE
        }
    }

    private fun inflateCurrentStage(currentStage: CurrentStage) {
        if (stageNameTextView != null) {
            stageNameTextView.text = currentStage.stage.name
            val durationMinutes = DurationUtils.toMinutes(currentStage.stage.durationMillis)
            durationTextView.text =
                requireActivity().getRussianResources().getQuantityString(
                    R.plurals.minutes,
                    durationMinutes,
                    durationMinutes
                )
            stageCard.setCardBackgroundColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.colorLightYellow
                )
            )
            stageProgressContainer.visibility = View.VISIBLE
            currentStageProgressBar.progress =
                DurationUtils.toSeconds(currentStage.stage.durationMillis - currentStage.getRemainMillis())
            currentStageProgressBar.max =
                DurationUtils.toSeconds(currentStage.stage.durationMillis)
        }
    }

    private fun startAnimator(currentStage: CurrentStage) {
        val remainMillis = currentStage.getRemainMillis()
        if (remainMillis > 0) {
            progressAnimator?.let {
                isProgressAnimatorCancelling = true
                it.cancel()
            }
            progressAnimator = ValueAnimator.ofInt(0, DurationUtils.toSeconds(remainMillis))
            with(progressAnimator!!) {
                duration = remainMillis
                addUpdateListener {
                    inflateCurrentStage(currentStage)
                }
                start()
            }
        }
    }

    override fun displayOfflineState() {}

    override fun displayOnlineState() {}
}
