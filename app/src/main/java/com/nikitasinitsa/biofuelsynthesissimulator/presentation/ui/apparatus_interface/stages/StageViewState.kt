package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages

enum class StageViewState {
    COMPLEATED, CURRENT, PENDING
}