package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.android.material.snackbar.Snackbar
import com.nikitasinitsa.biofuelsynthesissimulator.R
import com.nikitasinitsa.biofuelsynthesissimulator.device.NetworkStateProvider
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppMode
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_control.ControlFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.start_synthesis.StartSynthesisFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity(), MainActivityView, ConfigurableToolbar {

    private lateinit var presenter: MainActivityPresenter
    private var noConnectionSnackbar: Snackbar? = null
    private var currentToolbarState = ToolbarState.START_SYNTHESIS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        presenter = MainActivityPresenterImpl(appBridge, NetworkStateProvider(this))
        when (appBridge.prefs.mode) {
            AppMode.INTERFACE_MODE -> displayInterfaceMode()
            AppMode.CONTROL_MODE -> displayControlMode()
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val interfaceItem = menu.findItem(R.id.interfaceMenuEntry)
        val controlItem = menu.findItem(R.id.controlMenuEntry)
        when (currentToolbarState) {
            ToolbarState.START_SYNTHESIS -> {
                interfaceItem.isVisible = false
                controlItem.isVisible = true
            }
            ToolbarState.APPARATUS_INTERFACE -> {
                interfaceItem.isVisible = false
                controlItem.isVisible = true
            }
            ToolbarState.APPARATUS_CONTROL -> {
                interfaceItem.isVisible = true
                controlItem.isVisible = false
            }
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.controlMenuEntry -> {
                displayControlMode()
                true
            }
            R.id.interfaceMenuEntry -> {
                displayInterfaceMode()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun displayOfflineState() {
        if (noConnectionSnackbar == null) {
            noConnectionSnackbar = Snackbar.make(
                mainActivityRoot,
                getString(R.string.no_internet_connection),
                Snackbar.LENGTH_INDEFINITE
            ).apply { show() }
        }
    }

    override fun configureToolbar(state: ToolbarState) {
        currentToolbarState = state
        when (state) {
            ToolbarState.START_SYNTHESIS -> {
                toolbar.title = getString(R.string.app_name)
            }
            ToolbarState.APPARATUS_INTERFACE -> {
                toolbar.title = getString(R.string.interface_text)
            }
            ToolbarState.APPARATUS_CONTROL -> {
                toolbar.title = getString(R.string.control)
            }
        }
        invalidateOptionsMenu()
    }

    override fun displayOnlineState() {
        noConnectionSnackbar?.dismiss()
        noConnectionSnackbar = null
    }

    private fun displayInterfaceMode() {
        appBridge.prefs.mode = AppMode.INTERFACE_MODE
        supportFragmentManager.beginTransaction()
            .replace(mainActivityContainer.id, StartSynthesisFragment()).commit()
    }

    private fun displayControlMode() {
        appBridge.prefs.mode = AppMode.CONTROL_MODE
        supportFragmentManager.beginTransaction()
            .replace(mainActivityContainer.id, ControlFragment()).commit()
    }
}
