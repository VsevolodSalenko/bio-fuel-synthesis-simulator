package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base

import android.util.Log
import com.crashlytics.android.Crashlytics
import com.nikitasinitsa.biofuelsynthesissimulator.device.NetworkStateProvider
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

abstract class BasePresenterImpl<View : BaseView>(
    protected val appBridge: AppBridge,
    protected val networkStateProvider: NetworkStateProvider
) : Presenter<View> {
    protected val disposables = CompositeDisposable()
    private var viewReference: WeakReference<View>? = null

    protected fun getView(): View? = viewReference?.get()

    override fun onStart(view: View) {
        viewReference = WeakReference(view)
        disposables.add(
            networkStateProvider.observeNetworkState().subscribe { state ->
                when (state!!) {
                    NetworkStateProvider.NetworkState.CONNECTED -> onOnlineState()
                    NetworkStateProvider.NetworkState.NOT_CONNECTED -> onOfflineState()
                }
            }
        )
    }

    protected open fun onOfflineState() {
        getView()?.displayOfflineState()
    }

    protected open fun onOnlineState() {
        getView()?.displayOnlineState()
    }

    override fun onStop() {
        viewReference?.clear()
        viewReference = null
        disposables.clear()
    }

    protected fun showInternalError(error: Throwable) {
        Crashlytics.logException(error)
        Log.e("internalError", "", error)
        getView()?.showInternalError()
    }
}