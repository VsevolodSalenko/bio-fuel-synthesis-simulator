package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.synthesis_completed

import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.DialogPresenter

interface SynthesisCompletedFragmentPresenter : DialogPresenter<SynthesisCompletedFragmentView> {
    fun onFinishButtonClicked()
}