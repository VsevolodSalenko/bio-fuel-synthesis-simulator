package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main

interface ConfigurableToolbar {
    fun configureToolbar(state: ToolbarState)
}