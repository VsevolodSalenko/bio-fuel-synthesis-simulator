package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.start_synthesis

import com.nikitasinitsa.biofuelsynthesissimulator.device.NetworkStateProvider
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.ObserveCurrentStageUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BasePresenterImpl

class StartSynthesisFragmentPresenterImpl(
    appBridge: AppBridge,
    networkStateProvider: NetworkStateProvider
) : BasePresenterImpl<StartSynthesisFragmentView>(appBridge, networkStateProvider),
    StartSynthesisFragmentPresenter {

    private val observeCurrentStageUseCase = ObserveCurrentStageUseCase(appBridge.stagesRepository)

    override fun onOnlineState() {
        disposables.add(
            observeCurrentStageUseCase.execute(null).firstOrError().subscribe(
                {
                    if (it.stage.id == -1L) {
                        super.onOnlineState()
                    } else {
                        getView()?.showStagesScreen()
                    }
                },
                this::showInternalError
            )
        )
    }
}