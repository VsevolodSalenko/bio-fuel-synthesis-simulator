package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nikitasinitsa.biofuelsynthesissimulator.R
import com.nikitasinitsa.biofuelsynthesissimulator.domain.IdentifiableUtils
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.extensions.getRussianResources
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.utils.DurationUtils
import kotlinx.android.synthetic.main.item_stage.view.*


class StagesAdapter : RecyclerView.Adapter<StagesAdapter.StageViewHolder>() {
    private val stages = ArrayList<StageViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StageViewHolder =
        StageViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_stage,
                parent,
                false
            )
        )

    fun displayStages(updatedStages: List<StageViewModel>) {
        val copy = ArrayList(updatedStages)
        stages.clear()
        stages.addAll(copy)
        notifyDataSetChanged()
    }

    fun updateCurrentStage(updatedStage: StageViewModel) {
        stages.forEachIndexed { index, stage ->
            if (stage.state == StageViewState.CURRENT) {
                stage.update(updatedStage)
                notifyItemChanged(index)
            }
        }
    }

    fun switchStage(stage: StageViewModel) {
        val stageIndex = stages.indexOf(IdentifiableUtils.getById(stage.stage.id, stages))
        stages.forEachIndexed { index, displayingStage ->
            when {
                index < stageIndex -> {
                    displayingStage.state = StageViewState.COMPLEATED
                    displayingStage.progressSeconds = -1
                }
                index == stageIndex -> displayingStage.update(stage)
                else -> displayingStage.state = StageViewState.PENDING
            }
            notifyDataSetChanged()
        }
    }

    fun getCurrentStagePosition(): Int {
        stages.forEachIndexed { index, stage ->
            if (stage.state == StageViewState.CURRENT) {
                return index
            }
        }
        return -1
    }

    override fun getItemCount(): Int = stages.size

    override fun onBindViewHolder(holder: StageViewHolder, position: Int) {
        holder.bind(stages[holder.adapterPosition])
    }

    class StageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(model: StageViewModel) {
            with(itemView) {
                stageNameTextView.text = model.stage.name
                val durationMinutes = DurationUtils.toMinutes(model.stage.durationMillis)
                durationTextView.text =
                    itemView.context.getRussianResources().getQuantityString(
                        R.plurals.minutes,
                        durationMinutes,
                        durationMinutes
                    )

                when (model.state) {
                    StageViewState.COMPLEATED -> {
                        stageCard.setCardBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorLightGreen
                            )
                        )
                        stageProgressContainer.visibility = View.GONE
                    }
                    StageViewState.CURRENT -> {
                        stageCard.setCardBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorLightYellow
                            )
                        )
                        stageProgressContainer.visibility = View.VISIBLE
                        currentStageProgressBar.progress = model.progressSeconds
                        currentStageProgressBar.max =
                            DurationUtils.toSeconds(model.stage.durationMillis)

                    }
                    StageViewState.PENDING -> {
                        stageCard.setCardBackgroundColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.colorWhite
                            )
                        )
                        stageProgressContainer.visibility = View.GONE
                    }
                }
            }
        }
    }
}