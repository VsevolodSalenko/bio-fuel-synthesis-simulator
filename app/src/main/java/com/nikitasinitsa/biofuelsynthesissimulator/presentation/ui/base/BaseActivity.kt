package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.extensions.showInternalErrorToast

abstract class BaseActivity : AppCompatActivity(), BaseView {
    protected lateinit var appBridge: AppBridge

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appBridge = application as AppBridge
    }

    override fun showInternalError() = showInternalErrorToast()
}