package com.nikitasinitsa.biofuelsynthesissimulator.presentation.core

import android.content.Context

class Prefs(context: Context) {
    private companion object {
        private const val MODE_KEY = "mode"
    }

    private val preferences = context.getSharedPreferences("prefs", Context.MODE_PRIVATE)

    var mode: AppMode
        get() = AppMode.valueOf(
            preferences.getString(
                MODE_KEY,
                AppMode.INTERFACE_MODE.toString()
            )!!
        )
        set(mode) = preferences.edit().putString(MODE_KEY, mode.toString()).apply()

}