package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.error

import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.UpdateCurrentStageUseCase
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.core.AppBridge
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseDialogPresenterImpl

class ErrorFragmentPresenterImpl(appBridge: AppBridge) :
    BaseDialogPresenterImpl<ErrorFragmentView>(appBridge), ErrorFragmentPresenter {

    private val updateCurrentStageUseCase = UpdateCurrentStageUseCase(appBridge.stagesRepository)

    override fun onErrorFixed(currentStage: CurrentStage) {
        //todo update current stage start time
        currentStage.errorOccurredMillis = -1L
        disposables.add(
            updateCurrentStageUseCase.execute(currentStage).subscribe(
                {
                    getView()?.onErrorFixed()
                },
                ::showInternalError
            )
        )
    }
}