package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.start_synthesis

import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.Presenter

interface StartSynthesisFragmentPresenter : Presenter<StartSynthesisFragmentView> {
}