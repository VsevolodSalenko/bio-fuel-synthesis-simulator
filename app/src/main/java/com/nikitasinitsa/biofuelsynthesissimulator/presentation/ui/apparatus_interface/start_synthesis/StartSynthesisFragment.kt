package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.start_synthesis


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikitasinitsa.biofuelsynthesissimulator.R
import com.nikitasinitsa.biofuelsynthesissimulator.device.NetworkStateProvider
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.apparatus_interface.stages.StagesFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base.BaseFragment
import com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main.ToolbarState
import kotlinx.android.synthetic.main.fragment_start_synthesis.*

class StartSynthesisFragment : BaseFragment(), StartSynthesisFragmentView {
    override val toolbarState: ToolbarState = ToolbarState.START_SYNTHESIS
    private lateinit var presenter: StartSynthesisFragmentPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_start_synthesis, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter =
            StartSynthesisFragmentPresenterImpl(appBridge, NetworkStateProvider(requireActivity()))
        startButton.setOnClickListener { showStagesScreen() }
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun displayOfflineState() {
        startSynthesisProgressBar.visibility = View.VISIBLE
        readyToWorkContainer.visibility = View.INVISIBLE
    }

    override fun displayOnlineState() {
        startSynthesisProgressBar.visibility = View.INVISIBLE
        readyToWorkContainer.visibility = View.VISIBLE
    }

    override fun showStagesScreen() {
        fragmentManager!!.beginTransaction().replace(R.id.mainActivityContainer, StagesFragment())
            .commit()
    }
}
