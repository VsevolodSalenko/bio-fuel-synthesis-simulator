package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.main

enum class ToolbarState {
    START_SYNTHESIS, APPARATUS_INTERFACE, APPARATUS_CONTROL
}