package com.nikitasinitsa.biofuelsynthesissimulator.presentation.core

import androidx.multidex.MultiDexApplication
import com.nikitasinitsa.biofuelsynthesissimulator.data.FirestoreStagesRepository
import com.nikitasinitsa.biofuelsynthesissimulator.domain.repositories.StagesRepository

class App : MultiDexApplication(), AppBridge {
    override lateinit var stagesRepository: StagesRepository
    override lateinit var prefs: Prefs

    override fun onCreate() {
        super.onCreate()
        stagesRepository = FirestoreStagesRepository()
        prefs = Prefs(this)
    }
}