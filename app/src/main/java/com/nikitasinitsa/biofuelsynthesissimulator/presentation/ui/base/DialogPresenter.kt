package com.nikitasinitsa.biofuelsynthesissimulator.presentation.ui.base

interface DialogPresenter<View : BaseDialogView> {
    fun onStart(view: View)
    fun onStop()
}