package com.nikitasinitsa.biofuelsynthesissimulator.domain

object IdentifiableUtils {
    fun <T : Identifiable> getById(id: Long, list: List<T>): T? {
        list.forEach {
            if (it.id == id) {
                return it
            }
        }
        return null
    }
}