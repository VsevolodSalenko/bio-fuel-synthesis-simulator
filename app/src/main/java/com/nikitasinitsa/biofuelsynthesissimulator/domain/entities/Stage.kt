package com.nikitasinitsa.biofuelsynthesissimulator.domain.entities

import android.os.Parcel
import android.os.Parcelable
import com.nikitasinitsa.biofuelsynthesissimulator.domain.Identifiable
import com.nikitasinitsa.biofuelsynthesissimulator.domain.VersionComparable

data class Stage(
    override var id: Long = -1,
    var name: String = "",
    var durationMillis: Long = -1
) : Identifiable, VersionComparable<Stage>, Parcelable {

    override fun isChanged(anotherVersion: Stage): Boolean =
        name != anotherVersion.name || durationMillis != anotherVersion.durationMillis

    override fun update(anotherVersion: Stage) {
        name = anotherVersion.name
        durationMillis = anotherVersion.durationMillis
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Stage
        return id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    constructor(source: Parcel) : this(
        source.readLong(),
        source.readString()!!,
        source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeLong(id)
        writeString(name)
        writeLong(durationMillis)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Stage> = object : Parcelable.Creator<Stage> {
            override fun createFromParcel(source: Parcel): Stage = Stage(source)
            override fun newArray(size: Int): Array<Stage?> = arrayOfNulls(size)
        }
    }
}
