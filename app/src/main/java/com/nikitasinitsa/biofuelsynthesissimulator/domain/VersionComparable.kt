package com.nikitasinitsa.biofuelsynthesissimulator.domain

interface VersionComparable<T> {
    fun isChanged(anotherVersion: T): Boolean
    fun update(anotherVersion: T)
}