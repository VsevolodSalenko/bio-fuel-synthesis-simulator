package com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.base

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class ObservableUseCase<Params, Response> {
    abstract fun createObservable(params: Params): Observable<Response>

    fun execute(params: Params): Observable<Response> = applySchedulers(createObservable(params))

    private fun applySchedulers(observable: Observable<Response>) =
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}