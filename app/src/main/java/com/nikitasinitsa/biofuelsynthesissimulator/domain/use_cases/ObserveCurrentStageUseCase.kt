package com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases

import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.repositories.StagesRepository
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.base.ObservableUseCase
import io.reactivex.Observable

class ObserveCurrentStageUseCase(private val stagesRepository: StagesRepository) :
    ObservableUseCase<Void?, CurrentStage>() {
    override fun createObservable(params: Void?): Observable<CurrentStage> =
        stagesRepository.observeCurrentStage()
}