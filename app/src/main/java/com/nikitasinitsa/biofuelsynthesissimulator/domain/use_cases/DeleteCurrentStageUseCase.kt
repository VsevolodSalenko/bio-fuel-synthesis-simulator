package com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases

import com.nikitasinitsa.biofuelsynthesissimulator.domain.repositories.StagesRepository
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.base.CompletableUseCase
import io.reactivex.Completable

class DeleteCurrentStageUseCase(private val stagesRepository: StagesRepository) :
    CompletableUseCase<Void?>() {
    override fun createCompletable(params: Void?): Completable =
        stagesRepository.deleteCurrentStage()
}