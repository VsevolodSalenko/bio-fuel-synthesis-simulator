package com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases

import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.Stage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.repositories.StagesRepository
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.base.SingleUseCase
import io.reactivex.Single

class GetStagesUseCase(private val stagesRepository: StagesRepository) :
    SingleUseCase<Void?, List<Stage>>() {

    override fun createSingle(params: Void?): Single<List<Stage>> = stagesRepository.getStages()
}