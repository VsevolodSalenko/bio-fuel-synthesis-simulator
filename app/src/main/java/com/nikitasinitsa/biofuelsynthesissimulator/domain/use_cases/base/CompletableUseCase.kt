package com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.base

import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class CompletableUseCase<Params> {

    abstract fun createCompletable(params: Params): Completable

    fun execute(params: Params): Completable = applySchedulers(createCompletable(params))

    private fun applySchedulers(completable: Completable) =
        completable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}