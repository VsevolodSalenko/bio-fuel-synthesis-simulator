package com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases

import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.repositories.StagesRepository
import com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.base.CompletableUseCase
import io.reactivex.Completable

class UpdateCurrentStageUseCase(private val repository: StagesRepository) :
    CompletableUseCase<CurrentStage>() {
    override fun createCompletable(params: CurrentStage): Completable =
        repository.updateCurrentStage(params)
}