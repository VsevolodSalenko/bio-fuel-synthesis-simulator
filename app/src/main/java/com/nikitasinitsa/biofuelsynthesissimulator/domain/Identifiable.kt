package com.nikitasinitsa.biofuelsynthesissimulator.domain

interface Identifiable {
    var id: Long
}