package com.nikitasinitsa.biofuelsynthesissimulator.domain.use_cases.base

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class SingleUseCase<Params, Response> {
    abstract fun createSingle(params: Params): Single<Response>

    fun execute(params: Params): Single<Response> = applySchedulers(createSingle(params))

    private fun applySchedulers(single: Single<Response>) =
        single.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}