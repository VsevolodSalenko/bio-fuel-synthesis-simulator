package com.nikitasinitsa.biofuelsynthesissimulator.domain.repositories

import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.CurrentStage
import com.nikitasinitsa.biofuelsynthesissimulator.domain.entities.Stage
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface StagesRepository {
    fun getStages(): Single<List<Stage>>
    fun observeCurrentStage(): Observable<CurrentStage>
    fun updateCurrentStage(stage: CurrentStage): Completable
    fun deleteCurrentStage(): Completable
}