package com.nikitasinitsa.biofuelsynthesissimulator.domain.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.Exclude

data class CurrentStage(
    var stage: Stage = Stage(),
    var startMillis: Long = -1,
    var errorOccurredMillis: Long = -1
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as CurrentStage
        return stage == other.stage
    }

    override fun hashCode(): Int {
        return stage.hashCode()
    }

    @Exclude
    fun getRemainMillis() = (startMillis + stage.durationMillis) - System.currentTimeMillis()

    constructor(source: Parcel) : this(
        source.readParcelable<Stage>(Stage::class.java.classLoader)!!,
        source.readLong(),
        source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(stage, 0)
        writeLong(startMillis)
        writeLong(errorOccurredMillis)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CurrentStage> = object : Parcelable.Creator<CurrentStage> {
            override fun createFromParcel(source: Parcel): CurrentStage = CurrentStage(source)
            override fun newArray(size: Int): Array<CurrentStage?> = arrayOfNulls(size)
        }
    }
}

