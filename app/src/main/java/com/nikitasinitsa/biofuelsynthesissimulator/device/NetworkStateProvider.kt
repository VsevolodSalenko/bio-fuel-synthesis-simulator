package com.nikitasinitsa.biofuelsynthesissimulator.device

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo
import android.net.NetworkRequest
import android.os.Build
import androidx.annotation.RequiresApi
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables

class NetworkStateProvider(private val context: Context) {
    private val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private var receiver: BroadcastReceiver? = null
    private var networkCallback: ConnectivityManager.NetworkCallback? = null

    fun observeNetworkState(): Observable<NetworkState> =
        Observable.create<NetworkState> { emitter ->
            if (!emitter.isDisposed) {
                emitter.onNext(getNetworkStateInOldWay())
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                registerNetworkCallback(emitter)
            } else {
                registerBroadcastReceiver(emitter)
            }
            emitter.setDisposable(Disposables.fromAction(this::stopListening))
        }.observeOn(AndroidSchedulers.mainThread())

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun registerNetworkCallback(emitter: ObservableEmitter<NetworkState>) {
        networkCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network?) {
                if (!emitter.isDisposed) {
                    emitter.onNext(NetworkState.CONNECTED)
                }
            }

            override fun onLost(network: Network?) {
                if (!emitter.isDisposed) {
                    emitter.onNext(NetworkState.NOT_CONNECTED)
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(networkCallback)
        } else {
            val builder = NetworkRequest.Builder()
            connectivityManager.registerNetworkCallback(builder.build(), networkCallback)
        }
    }

    @Suppress("DEPRECATION")
    private fun registerBroadcastReceiver(emitter: ObservableEmitter<NetworkState>) {
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (!emitter.isDisposed) {
                    emitter.onNext(getNetworkStateInOldWay())
                }
            }
        }
        context.registerReceiver(receiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    @Suppress("DEPRECATION")
    private fun getNetworkStateInOldWay(): NetworkState {
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        return if (activeNetwork !== null && activeNetwork.isConnectedOrConnecting) {
            NetworkState.CONNECTED
        } else {
            NetworkState.NOT_CONNECTED
        }
    }


    private fun stopListening() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            networkCallback?.let {
                connectivityManager.unregisterNetworkCallback(it)
            }
            networkCallback = null
        } else {
            receiver?.let {
                context.unregisterReceiver(it)
            }
            receiver = null
        }
    }


    enum class NetworkState {
        CONNECTED, NOT_CONNECTED
    }
}